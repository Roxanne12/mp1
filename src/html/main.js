// Put your js code here
console.log("Hello World!");

/************ variables ****************/
var mods = document.getElementsByClassName("modbox");
var backs = document.getElementsByClassName("back");

var closes = document.getElementsByClassName("close");

//all the images
var images = document.getElementsByClassName("images");

// var introtop = getElementById("intro").offsetTop;
var navbarheight = document.getElementById("navbar").offsetHeight;
var topsecheight = document.getElementById("top").offsetHeight;

var factsoffset = topsecheight+document.getElementById("intro").offsetHeight;


var photooffset = factsoffset+document.getElementById("facts").offsetHeight;
// window.onscroll = function (e) {
//     console.log(window.scrollY); // Value of scroll Y in px
// };
//
window.onscroll= function(){
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("navbar").style.padding = "0.4%";
    for(var i = 0; i < document.getElementById("navbar").getElementsByTagName("a").length; i++){
    document.getElementById("navbar").getElementsByTagName("a")[i].style.fontSize = "100%";}

  } else {
    document.getElementById("navbar").style.padding = "1.2%";
    for(var i = 0; i < document.getElementById("navbar").getElementsByTagName("a").length; i++){
    document.getElementById("navbar").getElementsByTagName("a")[i].style.fontSize = "larger";}
  }

if(window.scrollY +navbarheight>= topsecheight && (window.scrollY +navbarheight< factsoffset) ){

// Value of scroll Y in px
var as = document.getElementById('navbar').getElementsByTagName("a");
  for(var i = 0; i < as.length; i++){
  as[i].classList.remove("active")
}
  document.getElementById("introa").classList.add("active");
}

else if(window.scrollY +navbarheight>= factsoffset && window.scrollY +navbarheight< photooffset ){

// Value of scroll Y in px
  var as = document.getElementById('navbar').getElementsByTagName("a");
    for(var i = 0; i < as.length; i++){
    as[i].classList.remove("active")
}
  document.getElementById("factsa").classList.add("active");
}


else if(window.scrollY +navbarheight>= photooffset){

// Value of scroll Y in px
  var as = document.getElementById('navbar').getElementsByTagName("a");
    for(var i = 0; i < as.length; i++){
    as[i].classList.remove("active")
}
  document.getElementById("photoa").classList.add("active");
}else{
  var as = document.getElementById('navbar').getElementsByTagName("a");
    for(var i = 0; i < as.length; i++){
    as[i].classList.remove("active")
}
}

}


/************ window ONLOAD  ****************/

window.onload = function(){
  //show the first image in the slider
  images[0].style.display = "block";
}

/************ Modal box section  ****************/
for(var i = 0; i < closes.length; i++){
  closes[i].addEventListener("click", closeModal, false);

}
function showModal(num){
  // var frontid = "front" + num.toString();
  // var frontid = getElementById(frontid);
  backs[num].style.display = "flex";
}

function closeModal(){
  for(var i = 0; i < backs.length; i++){
    if(backs[i].style.display != "none"){
      backs[i].style.display = "none";
    }
  }
}



/************ photo slider section  ****************/
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("images");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  slides[slideIndex-1].style.display = "block";
}
